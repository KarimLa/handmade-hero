#include <windows.h>
#include <stdint.h>
#include <Xinput.h>
#include <dsound.h>

#define internal static
#define local_persist static
#define global_variable static

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef int32 bool32;

struct win32_offscreen_buffer
{
  // NOTE(karim): Pixels are always 32-bits wide, Memory Order BB GG RR xx
  BITMAPINFO Info;
  void *Memory;
  int Width;
  int Height;
  int Pitch;
};

// TODO(karim): this is a global for now.
global_variable bool GlobalRunning;
global_variable win32_offscreen_buffer GlobalBackBuffer;
global_variable LPDIRECTSOUNDBUFFER GlobalSecondaryBuffer;

struct win32_window_dimension
{
  int Width;
  int Height;
};

// This helps us load function directly from dll with pointers instead of asking
// Windows executable loader to do anything with it.
// NOTE(karim): XInputGetState
#define X_INPUT_GET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_STATE *pState)
typedef X_INPUT_GET_STATE(x_input_get_state);
X_INPUT_GET_STATE(XInputGetStateStub)
{
  return (ERROR_DEVICE_NOT_CONNECTED);
}
global_variable x_input_get_state *XInputGetState_ = XInputGetStateStub;
#define XInputGetState XInputGetState_

// NOTE(karim): XInputSetState
#define X_INPUT_SET_STATE(name) DWORD WINAPI name(DWORD wUserIndex, XINPUT_VIBRATION *pVibration)
typedef X_INPUT_SET_STATE(x_input_set_state);
X_INPUT_SET_STATE(XInputSetStateStub)
{
  return (ERROR_DEVICE_NOT_CONNECTED);
}
global_variable x_input_set_state *XInputSetState_ = XInputSetStateStub;
#define XInputSetState XInputSetState_

// NOTE(karim): CreateDirectSound
#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);

internal void Win32LoadXInput(void)
{
  // TODO(karim): test this on windows 8
  HMODULE XInputLibrary = LoadLibraryA("xinput1_4.dll");
  if (!XInputLibrary)
  {
    XInputLibrary = LoadLibraryA("xinput1_3.dll");
  }

  if (XInputLibrary)
  {
    XInputGetState = (x_input_get_state *)GetProcAddress(XInputLibrary, "XInputGetState");
    if (!XInputGetState)
    {
      XInputGetState = XInputGetStateStub;
    }
    else
    {
      // TODO(karim): Diagnostic
    }

    XInputSetState = (x_input_set_state *)GetProcAddress(XInputLibrary, "XInputSetState");
    if (!XInputSetState)
    {
      XInputSetState = XInputSetStateStub;
    }
    else
    {
      // TODO(karim): Diagnostic
    }
  }
}

internal void Win32InitDSound(HWND Window, int32 SamplesPerSecond, int32 BufferSize)
{
  // NOTE(karim): Load the library
  HMODULE DsoundLibary = LoadLibraryA("dsound.dll");
  if (DsoundLibary)
  {
    // NOTE(karim): Get a DirectSound object
    direct_sound_create *DirectSoundCreate = (direct_sound_create *)GetProcAddress(DsoundLibary, "DirectSoundCreate");

    // TODO(karim): Double-check that this works on XP - DirectSound8 or 7??
    LPDIRECTSOUND DirectSound;
    if (DirectSoundCreate && SUCCEEDED(DirectSoundCreate(0, &DirectSound, 0)))
    {
      WAVEFORMATEX WaveFormat = {};
      WaveFormat.wFormatTag = WAVE_FORMAT_PCM;
      WaveFormat.nChannels = 2;
      WaveFormat.nSamplesPerSec = SamplesPerSecond;
      WaveFormat.wBitsPerSample = 16;
      WaveFormat.nBlockAlign = (WaveFormat.nChannels * WaveFormat.wBitsPerSample) / 8;
      WaveFormat.nAvgBytesPerSec = WaveFormat.nSamplesPerSec * WaveFormat.nBlockAlign;
      WaveFormat.cbSize = 0;

      if (SUCCEEDED(DirectSound->SetCooperativeLevel(Window, DSSCL_PRIORITY)))
      {
        DSBUFFERDESC BufferDescription = {};
        BufferDescription.dwSize = sizeof(BufferDescription);
        BufferDescription.dwFlags = DSBCAPS_PRIMARYBUFFER;

        // NOTE(karim): "Create" a primary buffer (to set its modes)
        // TODO(karim): DSBCAPS GLOBALFOCUS?
        LPDIRECTSOUNDBUFFER PrimaryBuffer;
        if (SUCCEEDED(DirectSound->CreateSoundBuffer(&BufferDescription, &PrimaryBuffer, 0)))
        {
          if (SUCCEEDED(PrimaryBuffer->SetFormat(&WaveFormat)))
          {
            // NOTE(karim): We have finally set the format
            OutputDebugStringA("PrimaryBuffer format was set");
          }
          else
          {
            // TODO(karim): Diagnostic
          }
        }
        else
        {
          // TODO(karim): Diagnostic
        }
      }
      else
      {
        // TODO(karim); Diagnostic
      }
      // TODO(karim): DSBCAPS_GETCURRENTPOSITION2
      DSBUFFERDESC BufferDescription = {};
      BufferDescription.dwSize = sizeof(BufferDescription);
      BufferDescription.dwFlags = 0;
      BufferDescription.dwBufferBytes = BufferSize;
      BufferDescription.lpwfxFormat = &WaveFormat;

      // NOTE(karim): "Create" a secondary buffer that we can write to
      HRESULT Error = DirectSound->CreateSoundBuffer(&BufferDescription, &GlobalSecondaryBuffer, 0);
      if (SUCCEEDED(Error))
      {
        OutputDebugStringA("SecondaryBuffer format was set");
      }
      else
      {
        // TODO(karim): Diagnostic
        OutputDebugStringA("SecondaryBuffer format was set");
      }
    }
    else
    {
      // TODO(karim): Diagnostic
    }
  }
  else
  {
    // TODO(karim): Diagnostic
  }
}

internal win32_window_dimension Win32GetWindowDimension(HWND Window)
{
  RECT ClientRect;
  win32_window_dimension Dimension;

  GetClientRect(Window, &ClientRect);

  Dimension.Width = ClientRect.right - ClientRect.left;
  Dimension.Height = ClientRect.bottom - ClientRect.top;

  return (Dimension);
}

internal void RenderWeirdGradient(win32_offscreen_buffer *Buffer, int BlueOfsset, int GreenOffset)
{
  // TODO(karim): let's see what the opimizer does
  uint8 *Row = (uint8 *)Buffer->Memory;

  for (int y = 0; y < Buffer->Height; ++y)
  {
    uint32 *Pixel = (uint32 *)Row;

    for (int x = 0; x < Buffer->Width; ++x)
    {
      uint8 Blue = (x + BlueOfsset);
      uint8 Green = (y + GreenOffset);

      *Pixel++ = ((Green << 8) | Blue);
    }

    Row += Buffer->Pitch;
  }
}

// NAME:        DBI -> Device Indepandent Bitmap
// DEFINITION:  The name that Windows uses to talk about things
// that you can write into as bitmaps that it can then display
// using GDI
internal void Win32ResizeDIBSection(win32_offscreen_buffer *Buffer, int Width, int Height)
{
  // TODO(karim): Bulletproof this.
  // Maybe don't free first, free after, then free first if that fails.

  if (Buffer->Memory)
  {
    VirtualFree(Buffer->Memory, 0, MEM_RELEASE);
  }

  Buffer->Width = Width;
  Buffer->Height = Height;
  int BytePerPixel = 4;

  // NOTE(karim): When the biHeight field is negative, this is the clue to
  // Windows to treat this bitmap as top-down, not bottom-up, meaning that
  // the first three bytes of the image are the color for the top left pixel
  // in the bitmap, not the bottom left!
  Buffer->Info.bmiHeader.biSize = sizeof(Buffer->Info.bmiHeader);
  Buffer->Info.bmiHeader.biWidth = Buffer->Width;
  Buffer->Info.bmiHeader.biHeight = -Buffer->Height;
  Buffer->Info.bmiHeader.biPlanes = 1;
  Buffer->Info.bmiHeader.biBitCount = 32;
  Buffer->Info.bmiHeader.biCompression = BI_RGB;

  int BitmapMemorySize = (Buffer->Width * Buffer->Height) * BytePerPixel;
  Buffer->Memory = VirtualAlloc(0, BitmapMemorySize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

  Buffer->Pitch = Width * BytePerPixel;
}

internal void Win32DisplayBufferInWindow(win32_offscreen_buffer *Buffer, HDC DeviceContext, int WindowWidth, int WindowHeight)
{
  // TODO(karim): Aspect ratio correction
  StretchDIBits(
      DeviceContext,
      // X, Y, Width, Height,
      // X, Y, Width, Height,
      0, 0, WindowWidth, WindowHeight,
      0, 0, Buffer->Width, Buffer->Height,
      Buffer->Memory,
      &Buffer->Info,
      DIB_RGB_COLORS,
      SRCCOPY);
}

internal LRESULT CALLBACK Win32MainWindowCallback(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
  LRESULT Result = 0;
  switch (Message)
  {
  case WM_DESTROY:
  {
    // TODO(karim): Handle this as a error - recreate window?
    GlobalRunning = false;
  }
  break;

  case WM_CLOSE:
  {
    // TODO(karim): Handle this with a messge to the user
    GlobalRunning = false;
  }
  break;

  case WM_ACTIVATEAPP:
  {
    OutputDebugStringA("WM_ACTIVATEAPP\n");
  }
  break;

  case WM_SYSKEYDOWN:
  case WM_SYSKEYUP:
  case WM_KEYDOWN:
  case WM_KEYUP:
  {
    uint32 VKCode = WParam;
    // LParam is bit field and it if it's set to 30th bit it tells wether the key used to be down or up
    bool WasDown = ((LParam & (1 << 30)) != 0);
    bool IsDown = ((LParam & (1 << 31)) == 0);

    if (IsDown != WasDown)
    {
      if (VKCode == 'W')
      {
      }
      else if (VKCode == 'A')
      {
      }
      else if (VKCode == 'S')
      {
      }
      else if (VKCode == 'D')
      {
      }
      else if (VKCode == 'Q')
      {
      }
      else if (VKCode == 'E')
      {
      }
      else if (VKCode == VK_UP)
      {
      }
      else if (VKCode == VK_DOWN)
      {
      }
      else if (VKCode == VK_LEFT)
      {
      }
      else if (VKCode == VK_RIGHT)
      {
      }
      else if (VKCode == VK_SPACE)
      {
      }
      else if (VKCode == VK_ESCAPE)
      {
        OutputDebugStringA("ESCAPE: ");
        if (IsDown)
        {
          OutputDebugStringA("IsDown");
        }
        if (WasDown)
        {
          OutputDebugStringA("WasDown");
        }
        OutputDebugStringA("\n");
      }
    }

    bool32 AltkeyWasDown = (LParam & (1 << 29));
    if ((VKCode == VK_F4) && AltkeyWasDown)
    {
      GlobalRunning = false;
    }
  }
  break;

  case WM_PAINT:
  {
    PAINTSTRUCT Paint;
    HDC DeviceContext = BeginPaint(Window, &Paint);

    win32_window_dimension Dimension = Win32GetWindowDimension(Window);
    Win32DisplayBufferInWindow(&GlobalBackBuffer, DeviceContext, Dimension.Width, Dimension.Height);
    EndPaint(Window, &Paint);
  }
  break;
  default:
  {
    // OutputDebugStringA("DEFAULT\n");
    Result = DefWindowProc(Window, Message, WParam, LParam);
  }
  break;
  }

  return (Result);
}

int CALLBACK WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nShowCmd)
{
  Win32LoadXInput();

  WNDCLASSA WindowClass = {};

  Win32ResizeDIBSection(&GlobalBackBuffer, 1280, 720);

  WindowClass.style = CS_HREDRAW | CS_VREDRAW;
  WindowClass.lpfnWndProc = Win32MainWindowCallback;
  WindowClass.hInstance = hInstance;
  // WindowClass.hIcon;
  WindowClass.lpszClassName = "HandmadeHeroWindowClass";

  if (RegisterClassA(&WindowClass))
  {
    HWND Window = CreateWindowExA(
        0,
        WindowClass.lpszClassName,
        "Handmade Hero",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        0,
        0,
        hInstance,
        0);

    if (Window)
    {
      HDC DeviceContext = GetDC(Window);

      // Note(karim): graphic test
      int XOffset = 0;
      int YOffset = 0;

      // NOTE(karim): Sound test
      int SamplesPerSecond = 48000;
      int ToneHz = 256;
      int16 ToneVolume = 6000;
      uint32 RunningSampleIndex = 0;
      int SquareWavePeriod = SamplesPerSecond / ToneHz;
      int HalfSquareWavePeriod = SquareWavePeriod / 2;
      int BytesPerSample = sizeof(int16) * 2;
      int SecondaryBufferSize = SamplesPerSecond * BytesPerSample;
      bool32 isSoundPlaying = false;

      Win32InitDSound(Window, SamplesPerSecond, SecondaryBufferSize);

      GlobalRunning = true;
      while (GlobalRunning)
      {
        MSG Message;

        while (PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
        {

          if (Message.message == WM_QUIT)
          {
            GlobalRunning = false;
          }

          TranslateMessage(&Message);
          DispatchMessage(&Message);
        }

        // TODO(karim): Should we poll this more frequently
        for (DWORD ControllerIndex = 0; ControllerIndex < XUSER_MAX_COUNT; ++ControllerIndex)
        {
          XINPUT_STATE ControllerState;
          if (XInputGetState(ControllerIndex, &ControllerState) == ERROR_SUCCESS)
          {
            // NOTE(karim): This controller is plugged in
            // TODO(karim): See if ControllerState.dwPacketNumber increments too rapidly
            XINPUT_GAMEPAD *Pad = &ControllerState.Gamepad;

            bool Up = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_UP);
            bool Down = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
            bool Left = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_LEFT);
            bool Right = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
            bool Start = (Pad->wButtons & XINPUT_GAMEPAD_START);
            bool Back = (Pad->wButtons & XINPUT_GAMEPAD_BACK);
            bool LeftShoulder = (Pad->wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
            bool RightShoulder = (Pad->wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);
            bool AButton = (Pad->wButtons & XINPUT_GAMEPAD_A);
            bool BButton = (Pad->wButtons & XINPUT_GAMEPAD_B);
            bool XButton = (Pad->wButtons & XINPUT_GAMEPAD_X);
            bool YButton = (Pad->wButtons & XINPUT_GAMEPAD_Y);

            int16 StickX = Pad->sThumbLX;
            int16 StickY = Pad->sThumbLY;

            XOffset += StickX >> 12;
            YOffset += StickY >> 12;
          }
          else
          {
            // NOTE(karim): this controller is not available
          }
        }

        RenderWeirdGradient(&GlobalBackBuffer, XOffset, YOffset);

        // NOTE(karim): DirectSound output test
        DWORD PlayCursor;
        DWORD WriteCursor;
        if (SUCCEEDED(GlobalSecondaryBuffer->GetCurrentPosition(&PlayCursor, &PlayCursor)))
        {
          DWORD ByteToLock = RunningSampleIndex * BytesPerSample % SecondaryBufferSize;
          DWORD BytesToWrite;
          if (ByteToLock == BytesToWrite)
          {
            BytesToWrite = SecondaryBufferSize;
          }
          else if (ByteToLock > PlayCursor)
          {
            BytesToWrite = SecondaryBufferSize - ByteToLock;
            BytesToWrite += PlayCursor;
          }
          else
          {
            BytesToWrite = PlayCursor - ByteToLock;
          }

          // TODO(karim): More strenuous tests
          // TODO(karim): Switch to a sine wave
          VOID *Region1;
          DWORD Region1Size;
          VOID *Region2;
          DWORD Region2Size;

          if (SUCCEEDED(GlobalSecondaryBuffer->Lock(ByteToLock, BytesToWrite, &Region1, &Region1Size, &Region2, &Region2Size, 0)))
          {
            // TODO(karim): assert that Region1Size/Region2Size is valid
            // TODO(karim): Collapse these two loops
            DWORD Region1SampleCount = Region1Size / BytesPerSample;
            int16 *SampleOut = (int16 *)Region1;
            for (DWORD SampleIndex = 0; SampleIndex < Region1SampleCount; ++SampleIndex)
            {
              int16 SampleValue = ((RunningSampleIndex++ / HalfSquareWavePeriod) % 2) ? ToneVolume : -ToneVolume;
              *SampleOut++ = SampleValue;
              *SampleOut++ = SampleValue;
            }

            DWORD Region2SampleCount = Region2Size / BytesPerSample;
            SampleOut = (int16 *)Region2;
            for (DWORD SampleIndex = 0; SampleIndex < Region2SampleCount; ++SampleIndex)
            {
              int16 SampleValue = ((RunningSampleIndex++ / HalfSquareWavePeriod) % 2) ? ToneVolume : -ToneVolume;
              *SampleOut++ = SampleValue;
              *SampleOut++ = SampleValue;
            }

            GlobalSecondaryBuffer->Unlock(Region1, Region1Size, Region2, Region2Size);
          }
        }

        if (!isSoundPlaying)
        {
          GlobalSecondaryBuffer->Play(0, 0, DSBPLAY_LOOPING);
          isSoundPlaying = true;
        }

        win32_window_dimension Dimension = Win32GetWindowDimension(Window);
        Win32DisplayBufferInWindow(&GlobalBackBuffer, DeviceContext, Dimension.Width, Dimension.Height);
      }
    }
    else
    {
      //  TODO(karim): LOGING
    }
  }
  else
  {
    // TODO(karim): LOGING
  }

  return (0);
}
